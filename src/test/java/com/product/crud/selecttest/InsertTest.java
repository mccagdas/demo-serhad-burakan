package com.product.crud.selecttest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;
import javax.sql.DataSource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.product.crud.InsertSelectProductApplicationTests;

public class InsertTest extends InsertSelectProductApplicationTests {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private Filter springSecurityFilterChain;

	private MockMvc mockMvc;

	@Before
	public void beforeTest() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		
		this.jdbcTemplate.update("delete from Price");
		this.jdbcTemplate.update("delete from Product");
	}

	@Test
	//@WithMockUser(username="admin",password="admin",roles={"USER","admin"})
	public void insertTestMethodSuccess() throws Exception{
		
		for(int i=1;i<=100;i++){
		

			
			MvcResult mvcResult = this.mockMvc.perform(
					get("/create/"
					+i
					+ "/test"+i
					+"/detail"+i
					+"/"+i
					+"/k�rm�z�"+i
					+"/10.35"
					+"/11.11.2015"
					+"/11.12.2015/"
							))
					.andExpect(request().asyncStarted())
					//.andDo(MockMvcResultHandlers.print())
					.andReturn();

		ResultActions resultActions = mockMvc.perform(asyncDispatch(mvcResult));
			resultActions.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("success").value(true)).andExpect(jsonPath("message").value("success"))
					.andExpect(jsonPath("id").value(i)).andExpect(jsonPath("name").value("test" + i))
					.andExpect(jsonPath("detail").value("detail" + i));
			
			

		}
		
	}
@Test	
public void insertTestMethodError() throws Exception{
		
	MvcResult mvcResult = this.mockMvc.perform(
			get("/create/1/test/detail/5/k�rm�z�/10.35/11.11.2015/11.12.2015"))
			.andExpect(request().asyncStarted())
			//.andDo(MockMvcResultHandlers.print())
			.andReturn();

ResultActions resultActions = mockMvc.perform(asyncDispatch(mvcResult));
	resultActions.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("success").value(false));
		
		}

	@After
	public void afterTest() {

	}

}
