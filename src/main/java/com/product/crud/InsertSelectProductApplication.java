package com.product.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class InsertSelectProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsertSelectProductApplication.class, args);
       
    }
    
 
}
