package com.product.crud.selectrest.service;


import javax.sql.DataSource;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import com.product.crud.insertrest.loggingservice.LoggingService;
import com.product.crud.insertrest.mapper.ProductMapper;
import com.product.crud.logging.LogEntry;
import com.product.crud.product.Product;

@Service
public class SelectService implements SelectServiceInterface {


	private JdbcTemplate jdbcTemplate; 
	
	@Autowired
	LoggingService loggingService;
	
	 @Autowired
	    public void setDataSource(DataSource dataSource) {
	        this.jdbcTemplate = new JdbcTemplate(dataSource);
	    }
	
	public Product findById(long id,String ip){
		Product product=null;
		try{
		product=this.jdbcTemplate.queryForObject(
		        "select product.id,product.name,product.detail,product.color,product.size,price.id as priceid,price.productid,price.productprice,price.begindate,price.enddate	from product inner join price on product.id=price.productid where product.id = ? limit 1",
		        new Object[]{id},
		        new ProductMapper()); 	
		product.setSuccess(true);
		

		}catch(Exception e){
			product=new Product();
			product.setSuccess(false);
			product.setMessage(e.getMessage());
		}
		loggingService.save(new LogEntry(new ObjectId().toString(),"select" ,ip,product.isSuccess()));
		return product;
	}
	
	
	
}
